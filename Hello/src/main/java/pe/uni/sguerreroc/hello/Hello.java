/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package pe.uni.sguerreroc.hello;

/**
 *
 * @author Sebastián Alonso Guerrero Carrillo  <sebastian.guerrero.c@uni.pe>
 */
public class Hello {

    public static void main(String[] args) {
        System.out.println("Hello World!");
    }
}
